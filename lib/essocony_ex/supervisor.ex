defmodule EssoconyEx.Supervisor do
  use Supervisor
  def start_link(args) do
    Supervisor.start_link(__MODULE__, args)
  end
  def init(port) do
    workers = [
      worker(EssoconyEx.Web, [port]),
      worker(EssoconyEx.Articles, []),
    ]
    supervise(workers, strategy: :one_for_one)
  end
end
