defmodule EssoconyEx.Index do
  require EEx
  use Timex
  def init(_type, req, state) do
    {:ok, req, state}
  end
  def handle(req, state) do
    {:ok, articles} = EssoconyEx.Articles.all
    articles = Enum.map(articles, fn({_k, v}) -> v end)
    {:ok, req} = :cowboy_req.reply(200, [], template(articles: articles), req)
    {:ok, req, state}
  end
  def terminate(_type, _req, _state) do
    :ok
  end
  EEx.function_from_file :defp, :template, "lib/essocony_ex/index.eex", [:assigns]
end
