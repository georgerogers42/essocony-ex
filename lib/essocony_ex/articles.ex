defmodule EssoconyEx.Articles do
  use Timex
  use GenServer
  defstruct dict: HashDict.new, list: []
  def start_link do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end
  def init(_args) do
    p = "articles"
    dict = File.ls!(p) |> Stream.filter(&Regex.match?(~r/\.markdown$/, &1)) |> Stream.map(&load_file(p, &1)) |> Enum.into(HashDict.new)
    list = sort_articles(dict)
    {:ok, %EssoconyEx.Articles{dict: dict, list: list}}
  end
  def handle_call({:lookup, name}, _from, state) do
    {:reply, Dict.fetch(state.dict, name), state}
  end
  def handle_call(:all, _from, state) do
    {:reply, {:ok, state.list}, state}
  end
  def handle_call(_req, _from, state) do
    {:reply, {:error, :badmsg}, state}
  end

  def lookup(name) do
    GenServer.call(__MODULE__, {:lookup, name})
  end
  def all do
    GenServer.call(__MODULE__, :all)
  end

  defp load_file(base, fname) do
    {:ok, slurped} = File.read("#{base}/#{fname}")
    [data, contents] = Regex.split(~r/\n\n/, slurped, [parts: 2])
    mdata = Poison.decode!(data) |> Dict.update!("date", &DateFormat.parse!(&1, "{ISO}"))
    {mdata["slug"], %{metadata: mdata, contents: Earmark.to_html(contents)}}
  end
  defp cmp_date(a, b) do
    Timex.Date.compare(b, a) < 0
  end
  defp sort_articles(enum) do
    Enum.sort_by(enum, fn({_k, v}) -> v[:metadata]["date"] end, &cmp_date/2)
  end
end
