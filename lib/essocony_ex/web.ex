defmodule EssoconyEx.Web do
  def routes do
    [
      _: [
        {"/", EssoconyEx.Index, %{}},
        {"/article/:article", EssoconyEx.Article, %{}},
        {"/[...]", :cowboy_static, {:dir, "public"}},
      ],
    ]
  end
  def dispatch do
    :cowboy_router.compile(routes)
  end
  def start_link(port) do
    :cowboy.start_http(__MODULE__, 100, [port: port], [env: [dispatch: dispatch]])
  end
end
