defmodule EssoconyEx.Article do
  require EEx
  use Timex
  def init(_type, req, state) do
    {:ok, req, state}
  end
  def handle(req, state) do
    {title, req} = :cowboy_req.binding(:article, req)
    {:ok, article} = EssoconyEx.Articles.lookup(title)
    {:ok, req} = :cowboy_req.reply(200, [], template(article: article), req)
    {:ok, req, state}
  end
  def terminate(_type, _req, _state) do
    :ok
  end
  EEx.function_from_file :defp, :template, "lib/essocony_ex/article.eex", [:assigns]
end
