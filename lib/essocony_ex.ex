defmodule EssoconyEx do
  use Application
  defp port do
    String.to_integer(System.get_env("PORT") || "5000")
  end
  def start(:normal, _args) do
    EssoconyEx.Supervisor.start_link(port)
  end
end
