require(["jquery", "underscore"], function($, _) {
    "use strict";
    $(function() {
        var timeout;
        var $answer = $("#million-dollar-homes-answer");
        var orgtext = $answer.text();
        $("table#million-dollar-homes").on("click", "img", function(evt) {
            var self = this;
            var $self = $(self);
            $answer.text($self.attr("alt"));
            clearTimeout(timeout);
            timeout = setTimeout(function() {
                $answer.text(orgtext);
            }, 5000);
        });
    });
});
