{"title": "Why no zoning matters", "slug": "no-zoning", "date": "2015-08-12T10:01:00-05", "author": "George Rogers"}

Zoning is a great enabler of corruption.
Because of the subjective nature of the zoning process many methods of subtle intimidation are enabled.

In every other city can ban businesses from opening due to arbitrary political requirements. But in Houston the City cannot do so; because it lacks that necessary mechanism for corruption.

## So What?
Back in the fall of 2014 there was a fight over the Houston Equal Rights Ordinance or the so called HERO ordinance.
During the fight Mayor Parker was denied the subtle means of control that most other cities have, thereby giving churches victory over city hall.
